import 'package:flutter/material.dart';

class MainPage extends StatelessWidget {

  MainPage({this.email});

  final String email;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Route"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text(
                email,
                style: Theme.of(context).textTheme.display1,
            ),
            RaisedButton(
              onPressed: () {},
              child: Text('Go back!'),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () =>
            {},
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}