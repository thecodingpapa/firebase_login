import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

class ForgetPW extends StatefulWidget {

  @override
  _ForgetPWState createState() => _ForgetPWState();
}

class _ForgetPWState extends State<ForgetPW> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        title: const Text("Reset Password"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Form(
                key: _formKey,
                child: TextFormField(
                  controller: _emailController,
                  decoration: InputDecoration(
                      icon: Icon(Icons.account_circle), labelText: "Email"),
                  validator: (String value) {
                    if (value.isEmpty || !value.contains("@")) {
                      return 'Please enter your email address!';
                    }
                    return null;
                  },
                ),
              ),
            ),
            SizedBox(
              width: 300,
              height: 60,
              child: RaisedButton(
                color: Colors.blue[900],
                textColor: Colors.white,
                child: const Text('Send an email to reset Password.'),
                onPressed: () async {
                  await _auth
                      .sendPasswordResetEmail(email: _emailController.text)
                      .whenComplete(() {
                    final snackBar =
                    SnackBar(content: Text('Reset Password Email Sent!'));
                    Scaffold.of(context).showSnackBar(snackBar);
                  }).catchError((e) {
                    final snackBar =
                    SnackBar(content: Text('Please try again later!'));
                    Scaffold.of(context).showSnackBar(snackBar);
                  });
                },
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0)),
                elevation: 8,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
