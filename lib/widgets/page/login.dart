import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_login/helper/empty_page_route.dart';
import 'package:firebase_login/widgets/page/forget_pw.dart';
import 'package:firebase_login/widgets/page/main.dart';
import 'package:flutter/material.dart';
import "package:flare_flutter/flare_actor.dart";
import 'package:provider/provider.dart';

const double buttonHeight = 50;
const double commonGap = 24;
const double positionFromBottom = 150;

final FirebaseAuth _auth = FirebaseAuth.instance;

class BeautifulLoginPage extends StatelessWidget {
  BeautifulLoginPage({this.joinOrLogin = false});

  final joinOrLogin;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<Size>.value(value: MediaQuery.of(context).size),
        Provider<bool>.value(value: joinOrLogin),
      ],
      child: Scaffold(
        body: InputForm(),
      ),
    );
  }
}

class InputForm extends StatefulWidget {
  @override
  _InputFormState createState() => _InputFormState();
}

class _InputFormState extends State<InputForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = Provider.of<Size>(context);

    return Consumer<bool>(
        builder: (context, isJoin, child) => Stack(
              children: <Widget>[
                CustomPaint(
                  size: Size(size.width, size.height),
                  painter: LoginBackPainter(
                      width: size.width,
                      height: size.height,
                      isJoin: Provider.of<bool>(context)),
                ),
                _animatedLogo(size),
                Positioned(
                  left: 24,
                  right: 24,
                  bottom: positionFromBottom,
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(commonGap),
                    ),
                    elevation: 12,
                    child: Padding(
                      padding: const EdgeInsets.all(commonGap),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            TextFormField(
                              controller: _emailController,
                              decoration: InputDecoration(
                                  icon: Icon(Icons.account_circle),
                                  labelText: "Email"),
                              validator: (String value) {
                                if (value.isEmpty || !value.contains("@")) {
                                  return 'Please enter your email address!';
                                }
                                return null;
                              },
                            ),
                            Container(
                              height: 12,
                            ),
                            TextFormField(
                              controller: _passwordController,
                              decoration: InputDecoration(
                                  icon: Icon(Icons.vpn_key),
                                  labelText: "Password"),
                              obscureText: true,
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Please enter any password!';
                                }
                                return null;
                              },
                            ),
                            Container(
                              height: 12,
                            ),
                            GestureDetector(
                              onTap: (){
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ForgetPW()),
                                );
                              },
                              child: isJoin
                                  ? Container(
                                      height: 0,
                                    )
                                  : Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        "Forgot password?",
                                        style: TextStyle(color: Colors.black54),
                                        textAlign: TextAlign.start,
                                      ),
                                    ),
                            ),
                            Container(
                              height: isJoin ? 12 : 24,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: positionFromBottom - buttonHeight / 2,
                  left: 100,
                  right: 100,
                  child: GestureDetector(
                    onTap: () async {
                      if (_formKey.currentState.validate()) {
                        isJoin ? _register() : _login();
                      }
                    },
                    child: Card(
                      elevation: 14,
                      color: isJoin ? Colors.red[800] : Colors.blue[800],
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(buttonHeight / 2),
                      ),
                      child: Container(
                          alignment: Alignment.center,
                          height: buttonHeight,
                          width: 200,
                          child: Text(
                            isJoin ? "Join" : "Login",
                            textAlign: TextAlign.center,
                            textScaleFactor: 1.3,
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          )),
                    ),
                  ),
                ),
                _joinOrLoginText(isJoin: isJoin, context: context),
              ],
            ));
  }

  // Example code for registration.
  void _login() async {
    final FirebaseUser user = await _auth
        .signInWithEmailAndPassword(
      email: _emailController.text,
      password: _passwordController.text,
    )
        .catchError((e) {
      final snackBar = SnackBar(content: Text(e.toString()));
      Scaffold.of(context).showSnackBar(snackBar);
    });
    if (user != null) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => MainPage(
                  email: user.email,
                )),
      );
    } else {
      final snackBar = SnackBar(content: Text('Please try again later!'));
      Scaffold.of(context).showSnackBar(snackBar);
    }
  }

  // Example code for registration.
  void _register() async {
    final FirebaseUser user = await _auth
        .createUserWithEmailAndPassword(
      email: _emailController.text,
      password: _passwordController.text,
    )
        .catchError((e) {
      final snackBar = SnackBar(content: Text(e.toString()));
      Scaffold.of(context).showSnackBar(snackBar);
    });
    if (user != null) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => MainPage(
                  email: user.email,
                )),
      );
    } else {
      final snackBar = SnackBar(content: Text('Please try again later!'));
      Scaffold.of(context).showSnackBar(snackBar);
    }
  }
}

Widget _animatedLogo(Size size) {
  final String _animationName = "Preview2";
  return Positioned(
    left: 0,
    right: 0,
    top: size.height * 0.05,
    height: size.height * 0.45,
    child: FlareActor(
      "asset/blessing.flr",
      alignment: Alignment.bottomCenter,
      fit: BoxFit.contain,
      animation: _animationName,
    ),
  );
}

Widget _joinOrLoginText(
        {@required bool isJoin, @required BuildContext context}) =>
    Positioned(
      bottom: 36,
      left: 0,
      right: 0,
      child: GestureDetector(
        onTap: () {
          Route route = EmptyPageRoute(
              page: BeautifulLoginPage(
            joinOrLogin: !isJoin,
          ));
//          Route route = MaterialPageRoute(builder: (context) => BeautifulLoginPage(
//            joinOrLogin: !isJoin,
//          ));
          isJoin
              ? Navigator.of(context).pop()
              : Navigator.of(context).push(route);
        },
        child: Text(
          isJoin
              ? "Already Have an Account? Log in"
              : "Don't Have an Account? Create One",
          style: TextStyle(color: isJoin ? Colors.red[800] : Colors.blue[800]),
          textAlign: TextAlign.center,
        ),
      ),
    );

class LoginBackPainter extends CustomPainter {
  LoginBackPainter({this.width, this.height, this.isJoin});

  final double width;
  final double height;
  final bool isJoin;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()..color = isJoin ? Colors.red[900] : Colors.blue[900];
    canvas.drawCircle(Offset(width * 0.5, 0), height * 0.65, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
