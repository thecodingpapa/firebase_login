import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:firebase_login/widgets/page/login.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BeautifulLoginPage(),
    );
  }
}



