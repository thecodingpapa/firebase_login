import 'package:flutter/material.dart';

class JoinOrLogin extends ChangeNotifier{
  bool _join = false;
  get isJoin => _join;
  set setJoin(bool isJoin) {
    _join = isJoin;
    notifyListeners();
  }
}